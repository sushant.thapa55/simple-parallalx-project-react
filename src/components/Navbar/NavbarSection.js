import React from 'react'
import './Navbarsection.css'
function NavbarSection() {
  return (
    <>
    <ul className='nav-items'>
       <a id='it1' href="#home">Home</a>     
       <a  id='it2' href="#about">About Me</a>
       <a id='it3' href="#services">Services</a>
       <a id='it4' href="#resume">Resume</a>
       <a id='it5' href="#contact">Contact Me</a>
    </ul>


    </>
  )
}

export default NavbarSection