import React from 'react'
import NavbarSection from '../Navbar/NavbarSection'


import './Header.css'

function HeaderSection() {
  return (
    <section className='main-container'>
      <section className='sub-container'>
          <section className='web-logo'>
              <a href='/'><h3>Say No To Drugs</h3></a>

          </section>
          <section className='web-navbar'>

              <NavbarSection />
             
          </section>
          
      </section>
      <section className='gmail'>
          Email:sushant.thapa55@gmail.com

      </section>
      <section className='phone'>
          9803194440
      </section>
    </section>
  )
}

export default HeaderSection