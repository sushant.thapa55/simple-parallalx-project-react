
import React from "react";
import FooterSection from "./components/Footer/FooterSection";
import HeaderSection from "./components/Header/HeaderSection";
import WebBody from "./components/WebBody/WebBody";

function App() {
  return (
    <div>
   <HeaderSection />

   <WebBody />

   <FooterSection />
   </div>
  );
}

export default App;
